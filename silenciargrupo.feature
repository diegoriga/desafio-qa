Funcionalidade: Silenciar grupo
Como um usuário
Eu desejo silenciar um grupo
De modo que eu não receba alertas sonoros

 Cenário: Silenciando um grupo sem exibir notificações
 Dado que estou na tela de uma conversa em grupo
 E clico no botão "Opções".
 E clico no botão "Silenciar".
 E clico no Botão "Ok".
 Então o grupo será silenciado
 E não receberei notificações do mesmo


 Cenário: Silenciando um grupo, exibindo notificações
 Dado que estou na tela de uma conversa em grupo
 E clico no botão "Opções".
 E clico no botão "Silenciar".
 E seleciono o checkbox "Exibir Notificações"
 E clico no Botão "Ok".
 Então o grupo será silenciado
 E receberei notificações do mesmo
 