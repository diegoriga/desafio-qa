Funcionalidade: Desbloquear Contato
Como um usário
Eu quero desbloquear um contato
De modo que este contato consiga me enviar mensagens

Cenário: Desbloqueando um contato.
    Dado que estou na tela com a lista de contatos bloqueados
    E a lista possui pelo menos um contato bloqueado
    Quando eu selecionar um contato
    E clicar na opção "Desbloquear Contato"
    Entao o usuário será desbloqueado
    E não constará mais na lista de contatos bloqueados