Funcionalidade: Limpar chamadas
Como um usuário
Eu quero limpar o histórico de chamadas
De modo que não existam mais chamadas no histórico

Cenário: Limpando o histórico de chamadas
 Dado que estou na aba de chamadas
 E possuam chamadas no histórico
 Quando eu clicar no botão "Limpar Chamadas"
 E clicar no botão "Ok" na mensagem de confirmação
 Então as chamadas no histórico deverão ser excluídas
